# Generated by Django 2.2.4 on 2019-09-02 08:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='posts/images/', verbose_name='Изображение'),
        ),
    ]
