from rest_framework import serializers
from .models import Post, Comment


class PostSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Post
		fields = (
			'id',
			'url',
			'author',
			'title',
			'preview',
			'image',
			'content',
			'created',
			'updated',
			'is_published'
		)


class CommentSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Comment
		fields = (
			'id',
			'url',
			'post',
			'parent',
			'content',
			'created',
			'updated'
		)
