from django import forms
from posts.models import Topic, Post, Comment


class TopicForm(forms.ModelForm):

    class Meta:
        model = Topic
        fields = ('title', 'image', 'description', 'closed')
        exclude = ('author', 'updated', 'created')


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('author', 'title', 'preview', 'image', 'content')
        exclude = ('updated', 'created')


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('post', 'content')
        widgets = {'post': forms.HiddenInput()}
        exclude = ('created', 'updated')
