from django import forms
from django.contrib.auth.forms import UserCreationForm
from accounts.models import User
from django.utils.translation import ugettext_lazy as _


class DateWidget(forms.DateInput):
    """Widget for date input"""
    input_type = 'date'


class SignupForm(UserCreationForm):
    first_name  = forms.CharField(max_length=30, label=_('Имя'))
    last_name   = forms.CharField(max_length=30, label=_('Фамилия'))
    email       = forms.EmailField(label=_('Адрес электронной почты'), required=True)

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        )

    def save(self, commit=True):
        user = super(SignupForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user


class ProfileForm(forms.ModelForm):

    class Meta:
        model = User
        exclude = (
            'password',
            'is_superuser',
            'groups',
            'user_permissions',
            'is_staff',
            'last_login',
            'is_active',
        )
