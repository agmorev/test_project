from django.contrib import admin
from accounts.models import User

# from django.contrib.auth.admin import UserAdmin
# from django.utils.translation import ugettext_lazy as _


admin.site.register(User)

# @admin.register(User)
# class CustomUserAdmin(UserAdmin):
#     list_display = (
#         'email',
#         'last_name',
#         'first_name',
#         'middle_name',
#         'photo',
#         'location',
#         'birth_date',
#         'is_staff')
#     list_filter = ('is_staff', 'is_superuser', 'groups')
#     search_fields = ('first_name', 'last_name', 'email')
#     ordering = ('email',)
#     add_fieldsets = (
#         (None, {
#             'classes': ('wide',),
#             'fields': (
#                 'email',
#                 'password1',
#                 'password2',)}))
#     fieldsets = (
#         (None, {
#             'fields': (
#                 'email',
#                 'password',)}),
#         (_('Personal info'), {
#             'fields': (
#                 'last_name',
#                 'first_name',
#                 'middle_name',
#                 'photo',
#                 'location',
#                 'birth_date',)}),
#         (_('Permissions'), {
#             'fields': (
#                 'is_staff',
#                 'is_superuser',
#                 'groups',
#                 'user_permissions',)}),
#         (_('Important dates'), {
#             'fields': (
#                 'last_login',)}),
#     )
