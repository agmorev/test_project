Django>=2.2.3,<2.3.0
# for posts
django-ckeditor>=5.7.1,<5.8.0
django-filter>=2.2.0,<2.3.0
django-widget-tweaks>=1.4.5,<1.5.0
Pillow>=6.1.0,<6.2.0
celery>=4.3.0,<4.4.0
flower>=0.9.3,<0.9.9
redis>=3.3.8,<3.4.0
djangorestframework>=3.10.2,<3.11.0
# for scrapping news
beautifulsoup4>=4.8.0,<4.9.0
lxml>=4.4.1,<4.5.0
requests>=2.22.0,<2.23.0