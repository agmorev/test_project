# news/views.py

from django.shortcuts import render
import requests
from bs4 import BeautifulSoup


def scraped_data(request):
    # session = requests.Session()
    # session.headers = {"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}
    url = "https://itbusiness.com.ua/softnews"
    content = requests.get(url).content
    soup = BeautifulSoup(content, "html.parser")
    
    posts = soup.find_all('div', {'class':'td_module_10 td_module_wrap td-animation-stack'})
    news = []
    for post in posts:
        published = post.find('span',{'class':'td-post-date'}).text
        title = post.find('h3',{'class':'entry-title td-module-title'}).text
        link = post.find('a',{'class':'td-image-wrap'})['href']
        news.append([published, title, link])
    return render(request, "news/news-list.html", {'news': news })