from django import forms

class QuadraticForm(forms.Form):

    a = forms.IntegerField(label='Коэффициент a:', error_messages={'required': 'Поле является обязательным для заполнения'})
    b = forms.IntegerField(label='Коэффициент b:', help_text='Значение этого коэффициента не должно равняться 0, поскольку уравнение будет неполным и требует других исчислений', error_messages={'required': 'Поле является обязательным для заполнения'})
    c = forms.IntegerField(label='Коэффициент c:', error_messages={'required': 'Поле является обязательным для заполнения'})

    def clean_a(self):
        a = self.cleaned_data['a']
        if a == 0:
            raise forms.ValidationError('Коэффициент a не должен равняться 0')
        return a

class DegreeForm(forms.Form):
    error_css_class = 'error'
    required_css_class = 'required '

    num = forms.IntegerField(label = 'Число',
                             widget=forms.NumberInput(attrs={'class': 'text', 'placeholder': 'Введите число'}),
                             error_messages={'required': 'Поле является обязательным для заполнения'})
    deg = forms.IntegerField(label='Степень', widget=forms.NumberInput(attrs={'class': 'text', 'placeholder': 'Введите степень'}), required=False)

    def is_valid(self):
        data = super(DegreeForm, self).is_valid()
        for field in self.errors:
            self.fields[field].widget.attrs.update({'color': 'red'})
        return data
