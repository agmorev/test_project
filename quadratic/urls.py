#quadratic/urls.def

from django.urls import path
from .views import quadratic_solution, degree_solution

urlpatterns = [
    path('quadratic/', quadratic_solution, name = 'quadratic'),
    path('degree/', degree_solution, name = 'degree'),
]
