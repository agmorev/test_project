from django.shortcuts import render
from .forms import QuadraticForm, DegreeForm

def quadratic_solution(request):
    if request.POST:
        form = QuadraticForm(request.POST)
        if form.is_valid():
            a = form.cleaned_data['a']
            b = form.cleaned_data['b']
            c = form.cleaned_data['c']
            d = b*b - 4*a*c
            if d > 0:
                x1 = (-b + d**(1/2.0)) / (2 * a)
                x2 = (-b - d**(1/2.0)) / (2 * a)
                return render(request, 'quadratic.html', {'form': QuadraticForm(), 'success': True, 'a': a, 'b': b, 'c': c, 'x1': int(x1), 'x2': int(x2), 'd': d})
            elif d == 0:
                x = (-b + d**(1/2.0)) / (2 * a)
                return render(request, 'quadratic.html', {'form': QuadraticForm(), 'success': True, 'a': a, 'b': b, 'c': c, 'x': int(x), 'd': d})
            else:
                return render(request, 'quadratic.html', {'form': QuadraticForm(), 'success': True, 'a': a, 'b': b, 'c': c, 'd': d})
        else:
            return render(request, 'quadratic.html', {'form': form})
    return render(request, 'quadratic.html', {'form': QuadraticForm()})

def degree_solution(request):
    if request.POST:
        context = {}
        form = QuadraticForm(request.POST, prefix = 'form')
        form2 = DegreeForm(request.POST, prefix = 'form2')
        if not form.is_valid():
            context.update({'form': form})
        else:
            a = form.cleaned_data['a']
            b = form.cleaned_data['b']
            c = form.cleaned_data['c']
            try:
                d = b*b - 4*a*c
                if d > 0:
                    x1 = (-b + d**(1/2.0)) / (2 * a)
                    x2 = (-b - d**(1/2.0)) / (2 * a)
                    context.update({'form': form, 'a': a, 'b': b, 'c': c, 'x1': int(x1), 'x2': int(x2), 'd': d})
                elif d == 0:
                    x = (-b + d**(1/2.0)) / (2 * a)
                    context.update({'form': form, 'a': a, 'b': b, 'c': c, 'x': int(x), 'd': d})
                else:
                    context.update({'form': form, 'a': a, 'b': b, 'c': c, 'd': d})
            except:
                d = ''
                x = ''
                x1 = ''
                x2 = ''

        if not form2.is_valid():
            context.update({'form2': form2})
        else:
            num = form2.cleaned_data['num']
            deg = form2.cleaned_data['deg']
            try:
                result = num ** deg
            except:
                result = ''
            context.update({'form2': form2, 'num': num, 'deg': deg, 'result': result})

        return render(request, 'degree.html', context)
    return render(request, 'degree.html', {'form': QuadraticForm(prefix = 'form'), 'form2': DegreeForm(prefix = 'form2')})
